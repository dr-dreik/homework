# -*- coding: utf-8 -*-

# Создать модуль my_burger. В нем определить функции добавления ингредиентов:
#  - булочки
#  - котлеты
#  - огурчика
#  - помидорчика
#  - майонеза
#  - сыра
# В каждой функции выводить на консоль что-то вроде "А теперь добавим ..."

# В этом модуле создать рецепт двойного чизбургера (https://goo.gl/zA3goZ)
# с помощью фукций из my_burger и вывести на консоль.

# Создать рецепт своего бургера, по вашему вкусу.
# Если не хватает ингредиентов - создать соответствующие функции в модуле my_burger


import my_burger


def burger():
  print('Рецепты бургеров:')
  user_input = input(('1 - Чизбургер /  2 - Двойной Чизбургер / 3 - Фирменный ----> '))

  my_burger.bun1()
  my_burger.mustard()
  my_burger.cutlet()
  if user_input == '3':
    firm_burger()
  else:
    my_burger.cheese()
    if user_input == '2':
      my_burger.cutlet()
      my_burger.cheese()
    my_burger.pickles_and_onion()
    my_burger.ketchup()
    my_burger.bun2()


def firm_burger():
  my_burger.blue_cheese()
  my_burger.pickles_and_onion()
  my_burger.egg()
  my_burger.ketchup()
  my_burger.bun2()
  my_burger.chips()


burger()

# зачёт!
