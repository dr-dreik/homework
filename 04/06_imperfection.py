# Вы пришли на работу в контору по разработке игр, целевая аудитория — дети и их родители.
# У прошлого программиста было задание сделать две игры в одном приложении, чтобы пользователь мог выбирать одну из них.
# Однако программист, на место которого вы пришли, перед увольнением не успел сделать эту задачу
# и оставил только небольшой шаблон проекта. Используя этот шаблон,
# реализуйте игры «Камень, ножницы, бумага» и «Угадай число».
#
# Правила игры «Камень, ножницы, бумага»: программа запрашивает у пользователя строку и выводит,
# победил он или проиграл. Камень бьёт ножницы, ножницы режут бумагу, бумага кроет камень.
#
# Правила игры «Угадай число»: программа запрашивает у пользователя число до тех пор, пока он его не отгадает.
import random


def rock_paper_scissors():
    user_input = input('камень, ножницы, бумага, раз, два, три ---> ')
    variants = ('камень', 'ножницы', 'бумага', 'камень', 'ножницы', 'бумага')  # здесь нужен был 4-ый элемент,
    # чтобы после бумаги снова шел камень (i+1), а остальные элементы -  для равной вероятности  их выпадения
    machine_variant = random.choice(variants)
    for i in range(0, 3):
        if user_input == variants[i] and machine_variant == variants[i + 1]:

            print("У компьютера -", machine_variant, "/ у вас -", user_input, "/ вы выиграли, ура!")
            break

        elif user_input == variants[i + 1] and machine_variant == variants[i]:

            print("У компьютера -", machine_variant, "/ у вас -", user_input, "/ вы проиграли")
            break
        elif user_input != "камень" and user_input != 'ножницы' and user_input != 'бумага':
            print('вводите только слова "камень", "ножницы" или "бумага"')
            break

    if user_input == machine_variant:
        print("У компьютера -", machine_variant, "/ у вас -", user_input, '/ победила дружба!')


def guess_the_number():
    while True:
        user_input = int(input('Введите число от 1 до 10 ---> :'))
        machine_variant = random.randint(1, 10)
        if machine_variant != user_input:
            print(f"Не угадал! Компьютер загадал число {machine_variant}. Попробуйте еще раз:")
        else:
            print("ВЫ ВЫИГРАЛИ! Комьютер тоже загадал", machine_variant)
            break


def main_menu():
    user_input = input("Во что будем играть? \\  1 - 'Камень, ножницы, бумага' \\ 2 - 'Угадай число'  ---> ")
    if user_input == '1':
        rock_paper_scissors()
    elif user_input == '2':
        guess_the_number()
    else:
        print("такой игры у нас нет")


main_menu()

# зачёт!
