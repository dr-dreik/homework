"""
Задача 2. Студенты
Что нужно сделать

Реализуйте модель с именем Student, содержащую поля: «ФИ», «Номер группы», «Успеваемость» (список из пяти элементов).
Затем создайте список из десяти студентов (данные о студентах можете придумать свои или запросить их у пользователя)
и отсортируйте его по возрастанию среднего балла. Выведите результат на экран.

"""
from random import choice, randint


class Student:
    def __init__(self):
        self.fi = ""
        self.group = 0
        self.progress = []

    def avr_progress(self):
        if len(self.progress) == 0:
            return 0
        return sum(self.progress) / len(self.progress)

    def __str__(self):
        return f"{self.fi},\t{self.group},\t{self.avr_progress()}"


students = []
names = ["Vasya", "Petya", "Pupa", "Lupa", "Ravshan"]
groups = ['Maths',"English", 'IT']
for _ in range(10):
    student = Student()
    student.fi = choice(names)
    student.group = choice(groups)
    student.progress = [randint(1, 5) for x in range(5)]
    students.append(student)


students.sort(key=lambda student: student.avr_progress(), reverse=True)

for student in students:
    print(student)

# зачёт!
