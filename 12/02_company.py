"""
Компания
Что нужно сделать

Реализуйте иерархию классов, описывающих служащих в компании. На самом верху иерархии — класс Person,
который описывает человека именем, фамилией и возрастом. Все атрибуты этого класса являются приватными.

Далее идёт класс Employee и производные от него классы Manager, Agent и Worker.

Класс «Работник» должен иметь метод расчёта заработной платы, переопределённый в каждом из производных классов.
Заработная плата Manager постоянна и равна 13000, заработная плата Agent определяется как оклад 5000 + 5% от объёма
продаж, который хранится в специальном поле класса Agent, а заработная плата Worker определяется
как 100 * число отработанных часов, которое также хранится в отдельном поле.

В основной программе создайте список из девяти объектов: первые три — Manager, следующие три — Agent и последние три —
Worker. Выведите на экран величину заработной платы всех девяти служащих.



Что оценивается

Результат вычислений корректен.
Модели реализованы в стиле ООП, основной функционал описан в методах классов и в отдельных функциях.
При написании классов соблюдаются основные принципы ООП: инкапсуляция, наследование и полиморфизм.
Для получения и установки значений у приватных атрибутов используются сеттеры и геттеры.
Для создания нового класса на основе уже существующего используется наследование.
Сообщения о процессе получения результата осмыслены и понятны для пользователя.
Переменные, функции и собственные методы классов имеют значащие имена, а не a, b, c, d.
Классы и методы/функции имеют прописанную документацию.
"""
from random import choice


class Person:
	def __init__(self):
		self.__name = ''
		self.__surname = ''
		self.__age = ''
		self.salary = ''

	def set_name(self, name):
		self.__name = name

	def set_surname(self, surname):
		self.__surname = surname

	def set_age(self, age):
		self.__age = age

	def __str__(self):
		return f"{self.__name} {self.__surname}, его зп - {self.salary} "


class Employee(Person):
	# TODO - Здесь надо написать метод расчета зп, возвращающий например 0
	#  Это нужно для того, чтобы в дочерних классах использовали для расчета зп именно этот метод
	pass


class Manager(Employee):

	def salary_count(self):
		self.salary = 13000



class Agent(Employee):

	def __init__(self):
		super().__init__()
		self.sales_volume = 0

	def set_sales_volume(self, sales_volume):
		self.sales_volume = sales_volume

	def salary_count(self):
		self.salary = self.sales_volume * 0.05 + 5000



class Worker(Employee):

	def set_working_hours(self, working_hours):
		self.working_hours = working_hours

	def salary_count(self):
		self.salary = self.working_hours * 100




names = ["Вася", "Петя", "Пупа", "Лупа", "Равшан", 'Джамшуд']
surnames = ['Иванов', 'Петров', 'Васечкин', "Сидоров", "Пупкин"]
sales_volume = [10000, 50000, 100000, 2000000]
working_hours = [100, 200, 300, 500]
employee = []



print('|{txt:*^30}|'.format(txt='Агенты'))
for _ in range(3):
	agent = Agent()
	agent.set_name(choice(names))
	agent.set_surname(choice(surnames))
	agent.set_sales_volume(choice(sales_volume))
	agent.salary_count()
	employee.append(agent)
	print('Агент', agent)


print('|{txt:*^30}|'.format(txt='Менеджеры'))
for _ in range(3):
	manager = Manager()
	manager.set_name(choice(names))
	manager.set_surname(choice(surnames))
	manager.salary_count()
	employee.append(manager)
	print('Менеджер', manager)

print('|{txt:*^30}|'.format(txt='Работники'))
for _ in range(3):
	worker = Worker()
	worker.set_name(choice(names))
	worker.set_surname(choice(surnames))
	worker.set_working_hours(choice(working_hours))
	worker.salary_count()
	employee.append(worker)
	print('Работник', worker)

# зачёт!

