# Задание 2
# Реализуйте класс «Книга». Необходимо хранить в
# полях класса: название книги, год выпуска, издателя,
# жанр, автора, цену. Реализуйте методы класса для ввода
# данных, вывода данных, реализуйте доступ к отдельным
# полям через методы класса.

class Book:
	# TODO - Это лишнее
	__name = ''

	def __int__(self):
		self.__name = ""
		self.__year = 0
		self.__manufacturer = ''
		self.__genre = ''
		self.__price = 0
		self.__autor = ''

	def set_name(self, name):
		self.__name = name

	def get_name(self):
		return self.__name

	def set_year(self, year):
		self.__year = year

	def get_year(self):
		return self.__year

	def set_manufacturer(self, manuf):
		self.__manufacturer = manuf

	def get_manufacturer(self):
		return self.__manufacturer

	def set_genre(self, genre):
		self.__genre = genre

	def get_genre(self):
		return self.__genre

	def set_price(self, price):
		self.__price = price

	def set_autor(self, autor):
		self.__autor = autor

	def get_price(self):
		return self.__price

	def __str__(self):
		return f'The book I reading now is  {self.__name} by {self.__autor} {self.__year} year of issue by' \
		       f' {self.__manufacturer}  in genre  {self.__genre}   and it costs about {self.__price} rub'


book = Book()
book.set_name('KGBT+')
book.set_year(2022)
book.set_manufacturer('Exmo')
book.set_genre ('adventure & philosophy')
book.set_autor('Victor Pelevin')
book.set_price(700)

print(book)

# зачёт!
