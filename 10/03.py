# Задание 3
# Реализуйте класс «Стадион». Необходимо хранить в
# полях класса: название стадиона, дату открытия, страну,
# город, вместимость. Реализуйте методы класса для ввода
# данных, вывода данных, реализуйте доступ к отдельным
# полям через методы класса.


class Stadium:

	def __int__(self):
		# TODO - Забыли спрятать поля с помощью префикса "__"
		self.name = ""
		self.year = 0
		self.country = ''
		self.city = ''
		self.capacity = 0

	def set_name(self, name):
		self.name = name

	def get_name(self):
		return self.name

	def set_year(self, year):
		self.year = year

	def get_year(self):
		return self.year
	def set_country(self, country):
		self.country = country
	def get_country(self):
		return self.country

	def set_city(self, city):
		self.city=city

	def get_city(self):
		return (self.city)
	def set_capacity(self, capacity):
		self.capacity = capacity

	def get_capacity(self):
		return self.capacity

	def __str__(self):
		return f'Stadium `{self.name}`  was built in {self.year} year in {self.city}, {self.country}. Its capacity is ' \
		       f'{self.capacity} people'


stadium = Stadium()
stadium.set_name('Camp Nou')
stadium.set_year(1957)
stadium.set_country('Spain')
stadium.set_city ('Barcelona')
stadium.set_capacity(99354)

print(stadium)

# зачёт!
