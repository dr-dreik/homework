# Реализуйте класс «Автомобиль». Необходимо хранить
# в полях класса: название модели, год выпуска, произво-
# дителя, объем двигателя, цвет машины, цену. Реализуйте
# методы класса для ввода данных, вывода данных, реа-
# лизуйте доступ к отдельным полям через методы класса.

class Car:

	def __int__(self):
		# Спрячем все поля, чтобы снаружи не было их видно
		self.__model = ""
		self.__year = 0
		self.__manufacturer = ''
		self.__color = ''
		self.__price = 0
		self.__eng_vol = 0

	def get_model(self):
		return self.__model

	def set_model(self, model):
		self.__model = model

	def set_year(self, year):
		self.__year = year

	def get_year(self):
		return self.__year

	def set_manufacturer(self, manuf):
		self.__manufacturer = manuf

	def get_manufacturer(self, manuf):
		return self.__manufacturer

	def set_color(self, color):
		self.__color = color

	def get_color(self):
		return self.__color

	def set_price(self, price):
		self.__price = price

	def get_price(self):
		return self.__price

	def set_eng_volume(self, eng):
		self.__eng_vol = eng

	def get_eng_volume(self):
		return self.__eng_vol

	def __str__(self):
		return f'My car is {self.__model} {self.__year} year of issue by {self.__manufacturer} in {self.__color} ' \
		       f'color with {self.__eng_vol} engine and costs only {self.__price}$'


car = Car()
car.set_model('Opel')
car.set_year(2011)
car.set_manufacturer('General Motors')
car.set_color('black')
car.set_eng_volume(1.6)
car.set_price(1000000)

print(car)

# зачёт!
