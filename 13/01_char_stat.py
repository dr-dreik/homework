# -*- coding: utf-8 -*-

# Подсчитать статистику по буквам в романе Война и Мир.
# Входные параметры: файл для сканирования
# Статистику считать только для букв алфавита
#
# Вывести на консоль упорядоченную статистику в виде
# +---------+----------+
# |  буква  | частота  |
# +---------+----------+
# |    А    |   77777  |
# |    Б    |   55555  |
# +---------+----------+
# |  итого  | 9999999  |
# +---------+----------+
# Ширину таблицы подберите по своему вкусу


with open('voyna-i-mir.txt', mode='r', encoding='cp1251') as file:
	letters = {}
	letters_count = 0
	for line in file:
		for symb in line.lower():
			if symb.isalpha():
				if symb in letters:
					letters[symb] += 1
				else:
					letters[symb] = 1
	for symb in letters:
		letters_count += letters[symb]

sorted_letters = sorted(letters.items())
# sorted_letters = str(sorted_letters)
# str = sorted_letters.replace("(\'", '|   ')
# str = str.replace('\',', '    |')
# str = str.replace('), ', '\n')
# str=str.replace('[', '')
# str=str.replace(')]','')
# print(str)

print('''  +---------+----------+
  |  буква  | частота  |
  +---------+----------+''''')
for elem in sorted_letters:
	print('  |    {0}    |{1:8d}  |  '.format(elem[0], elem[1]))

print("""  +---------+----------+
  |  итого  |  {} |
  +---------+----------+""".format(letters_count))

# TODO - Буква "ё" оказалась последней. Это надо поправить
# зачёт!
