# -*- coding: utf-8 -*-

import os, time, shutil

# Нужно написать скрипт для упорядочивания фотографий (вообще любых файлов)
# Скрипт должен разложить файлы из одной папки по годам и месяцам в другую.
# Например, так:
#   исходная папка
#       photo/cat.jpg
#       photo/man.jpg
#       photo/new_year_01.jpg
#   результирующая папка
#       my_photo_archive/2018/05/cat.jpg
#       my_photo_archive/2018/05/man.jpg
#       my_photo_archive/2017/12/new_year_01.jpg
#
# Входные параметры: папка для сканирования, целевая папка
# Имена файлов не менять, год и месяц взять из времени создания файла.
# Исходная папка - icons, результирующая icons_by_years
#
# Пригодятся функции:
#   os.walk
#   os.path.dirname
#   os.path.join
#   os.path.normpath
#   os.path.getctime
#   time.gmtime
#   os.makedirs
#   shutil.copy2
#
# Чтение документации/гугла по функциям - приветствуется. Как и поиск альтернативных вариантов...
path = 'icons'
path2 = 'icons_by_year'

for dirpath, dirnames, filenames in os.walk(path):
	for file in filenames:
		full_file_path = os.path.join(dirpath, file)
		secs = os.path.getmtime(full_file_path)
		file_time = time.gmtime(secs)
		file_year = f'{file_time[0]}'
		file_month = f'{file_time[1]}'
		full_file_year_path = os.path.join(path2, file_year)
		os.makedirs(full_file_year_path, exist_ok=True)
		full_file_month_path = os.path.join(full_file_year_path, file_month)
		os.makedirs(full_file_month_path, exist_ok=True)
		shutil.copy2(full_file_path, full_file_month_path)

# зачёт!
