"""
Частотный анализ
Что нужно сделать

Есть файл text.txt, который содержит текст. Напишите программу, которая выполняет частотный анализ, определяя долю
каждой буквы английского алфавита в общем количестве английских букв в тексте, и выводит результат в файл analysis.txt.
Символы, не являющиеся буквами английского алфавита, учитывать не нужно.

В файл analysis.txt выводится доля каждой буквы, встречающейся в тексте, с тремя знаками в дробной части. Буквы должны
быть отсортированы по убыванию их доли. Буквы с равной долей должны следовать в алфавитном порядке.

Пример:

Содержимое файла text.txt:
Mama myla ramu.

Содержимое файла analysis.txt:
a 0.333
m 0.333
l 0.083
r 0.083
u 0.083
y 0.083
"""
start = -1
count = 0
# TODO - Не обязательно делать вложенные блоки with
#  Для записи в файл правильнее открыть в месте использования, а не заранее
with open('text.txt', 'r') as file:
	with open('analysis.txt', 'w') as file2:
		line = file.readline()
		letter_list = ''
		letter_dict = {}
		for symb in line:
			if symb.isalpha():
				letter_list += symb
			letter_list = letter_list.lower()
		for letter in letter_list:
			while True:
				start = letter_list.find(letter, start + 1)
				# TODO - letter_list.count(letter) - не подойдет?
				if start == -1:
					letter_dict[letter] = round(count / len(letter_list), 3)
					count = 0
					break
				count += 1

		sort_letter_dict = dict(sorted(letter_dict.items()))

		# TODO - Все импорты выносятся в начало кода
		from operator import itemgetter

		sort_letter_dict = dict(sorted(sort_letter_dict.items(), key=itemgetter(1), reverse=True))

		for elem in sort_letter_dict:
			line2 = f'{elem} {sort_letter_dict[elem]} \n'
			file2.write(line2)

# зачёт!
