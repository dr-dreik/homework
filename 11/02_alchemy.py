# -*- coding: utf-8 -*-

# Создать прототип игры Алхимия: при соединении двух элементов получается новый.
# Реализовать следующие элементы: Вода, Воздух, Огонь, Земля, Шторм, Пар, Грязь, Молния, Пыль, Лава.
# Каждый элемент организовать как отдельный класс.
# Таблица преобразований:
#   Вода + Воздух = Шторм
#   Вода + Огонь = Пар
#   Вода + Земля = Грязь
#   Воздух + Огонь = Молния
#   Воздух + Земля = Пыль
#   Огонь + Земля = Лава

# Сложение элементов реализовывать через __add__
# Вывод элемента на консоль  реализовывать через __str__
# Примеры преобразований:
#   print(water + fire)
#   print(air + water)

class Water:

	def __init__(self):
		self.name = 'Вода'

	def __add__(self, other):
		# TODO - Принты в классах не следует использовать. Весь вывод информации - вне классов
		print(f'{self.name} + {other.name}', end='')
		# TODO - Здесь air это объект. А надо сравнивать с классом объекта other. Через isinstance()
		if isinstance(other, Air):
			#  - А здесь надо создавать новый объект класса Storm
			# return storm
			# TODO - Где гарантия, что объект storm уже создан? Нет её. поэтому здесь на место создаем объект,
			# TODO - вот так (здесь и далее надо исправить)
			return Storm()

		# TODO - Далее аналогично всё нужно исправить
		elif isinstance(other, Fire):
			return steam
		elif isinstance(other, Ground):
			return mud

	def __str__(self):

		return self.name


class Fire:

	def __init__(self):
		self.name = 'Огонь'

	def __add__(self, other):
		print(f'{self.name} + {other.name}', end='')
		if isinstance(other, Air):
			return lightning
		elif isinstance(other, Ground):
			return lava
		elif isinstance(other, Water):
			return steam

	def __str__(self):
		return self.name


class Air:

	def __init__(self):
		self.name = 'Воздух'

	def __add__(self, other):
		if isinstance(other, Ground):
			return dust
		elif isinstance(other, Water):
			return storm
		elif isinstance(other, Fire):
			return lightning

	def __str__(self):
		return self.name


class Ground:
	def __init__(self):
		self.name = 'Земля'

	def __add__(self, other):
		print(f'{self.name} + {other.name}', end='')
		if isinstance(other, Air):
			return dust
		elif isinstance(other, Fire):
			return lava
		elif isinstance(other, Water):
			return mud


def __str__(self):
	return self.name


class Steam:

	def __init__(self):
		self.name = 'Пар'

	def __str__(self):
		return f' = {self.name}'


class Dust:

	def __init__(self):
		self.name = 'Пыль'

	def __str__(self):
		return f' = {self.name}'


class Lightning:

	def __init__(self):
		self.name = 'Молния'

	def __str__(self):
		return f' = {self.name}'


class Lava:

	def __init__(self):
		self.name = 'Лава'

	def __str__(self):
		return f' = {self.name}'


class Mud:

	def __init__(self):
		self.name = 'Грязь'

	def __str__(self):
		return f' = {self.name}'


class Storm:

	def __init__(self):
		self.name = 'Шторм'

	def __str__(self):
		return f' = {self.name}'


water = Water()
fire = Fire()
air = Air()
ground = Ground()
storm = Storm()
steam = Steam()
mud = Mud()
lightning = Lightning()
lava = Lava()
dust = Dust()

print(water + fire)
print(ground + air)
print(fire + ground)


# Усложненное задание (делать по желанию)
# Добавить еще элемент в игру.
# Придумать что будет при сложении существующих элементов с новым.
# Если результат не определен - то возвращать None

class Grass:
	def __init__(self):
		self.name = 'Трава'


	def __add__(self, other):
		print(f'{self.name} + {other.name}', end=' ')
		if isinstance(other,Water):
			return tea
		elif isinstance(other, Ground):
			return garden
		else:
			return None

class Tea:
	def __init__(self):
		self.name = 'Чай'

	def __str__(self):
		return f' = {self.name}'
class Garden:
	def __init__(self):
		self.name = 'Сад'

	def __str__(self):
		return f' = {self.name}'

tea = Tea()
grass = Grass()
garden = Garden()
print(grass + water)
print(grass + ground)
print(grass + fire)
