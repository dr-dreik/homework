# -*- coding: utf-8 -*-

from random import randint
from termcolor import cprint


# Необходимо создать класс кота. У кота есть аттрибуты - сытость и дом (в котором он живет).
# Кот живет с человеком в доме.
# Для кота дом характеризируется - миской для еды и грязью.
# Изначально в доме нет еды для кота и нет грязи.

# Доработать класс человека, добавив методы
#   подобрать кота - у кота появляется дом.
#   купить коту еды - кошачья еда в доме увеличивается на 50, деньги уменьшаются на 50.
#   убраться в доме - степень грязи в доме уменьшается на 30, сытость у человека уменьшается на 20.
# Увеличить кол-во зарабатываемых человеком денег до 150 (он выучил пайтон и устроился на хорошую работу :)

# Кот может есть, спать и драть обои - необходимо реализовать соответствующие методы.
# Когда кот спит - сытость уменьшается на 10
# Когда кот ест - сытость увеличивается на 20, кошачья еда в доме уменьшается на 20.
# Когда кот дерет обои - сытость уменьшается на 10, степень грязи в доме увеличивается на 10
# Если степень сытости < 0, кот умирает.
# Так же надо реализовать метод "действуй" для кота, в котором он принимает решение
# что будет делать сегодня

# Человеку и коту надо вместе прожить 365 дней.


class Human:

	def __init__(self, name):
		self.name = name
		self.fullness = 50
		self.house = None

	def __str__(self):
		return 'Я - {}, моя сытость {}'.format(
			self.name, self.fullness)

	def eat(self):
		if self.house.food >= 10:
			cprint('{} поел'.format(self.name), color='yellow')
			self.fullness += 10
			self.house.food -= 10
		else:
			cprint(' у {}а нет еды!'.format(self.name), color='red')
			self.fullness -= 10

	def work(self):
		cprint('{} сходил на работу'.format(self.name), color='blue')
		self.house.money += 150
		self.fullness -= 10

	def sleep(self):
		cprint('{}: сегодня я буду спать'.format(self.name), color='green')
		self.fullness -= 10

	def take_cat(self, house, cat):
		# TODO - надо получать в параметрах кота. Затем у этого кота поле house приравнять к своему полю hause
		#  Тем самым, конкретному коту присваивается дом подобравшего
		self.house = house
		cat.house = self.house
		cprint('{} принес в дом {}а'.format(self.name, cat.name), color='cyan')

	def shopping(self):
		if self.house.money >= 50:
			cprint('{} сходил в магазин себе за едой'.format(self.name), color='magenta')
			self.house.money -= 50
			self.house.food += 50
		else:
			cprint('У {}а  кончились деньги!'.format(self.name), color='red')

	def shopping_for_cat(self):
		if self.house.money >= 50:
			cprint('{} сходил коту за едой'.format(self.name), color='magenta')
			self.house.money -= 50
			self.house.cat_food += 50
		else:
			cprint('Прости кот, у  {}а деньги кончились'.format(self.name), color='red')

	def cleaning(self):
		# TODO - Если грязи было меньше 30, то отрицательное число получим. Такого не должно быть
		# todo - не получим, так как убираемся не рандомно
		self.house.dirty -= 30
		self.fullness -= 20
		cprint('{} прибрался за котом'.format(self.name), color='green')

	def act(self):
		if self.fullness <= 0:
			cprint('{} умер...'.format(self.name), color='red')
			return
		dice = randint(1, 6)
		if self.house.food < 10 and self.house.money > 50:
			self.shopping()
		elif self.fullness < 30:
			self.eat()
		elif self.house.cat_food < 40 and self.house.money > 50:
			self.shopping_for_cat()
		elif self.house.money < 151:
			self.work()
		elif self.house.dirty >= 30:
			self.cleaning()
		elif dice == 1:
			self.shopping_for_cat()
		elif dice == 2:
			self.eat()
		elif dice == 3:
			self.shopping_for_cat()
		elif dice == 4:
			self.shopping()
		elif dice == 5:
			self.sleep()
		else:
			self.work()


class Cat:
	def __init__(self, name):

		self.name = name
		self.fullness = 50
		self.house = None

	def get_home(self, house):
		self.house = house
		cprint('{} поселился в доме'.format(self.name), color='cyan')

	def eat(self):
		if self.house.cat_food >= 20:
			cprint('{} поел'.format(self.name), color='yellow')
			self.fullness += 20
			self.house.cat_food -= 20
		else:
			cprint('у {}а нет еды'.format(self.name), color='red')
			self.fullness -= 5 # кот переходит в энергосберегающий режим

	def __str__(self):
		return 'Я - {}, моя сытость {}'.format(
			self.name, self.fullness)

	def sleep(self):
		self.fullness -= 10
		cprint('{} спал весь день'.format(self.name), color='yellow')

	def scratch(self):
		self.house.dirty += 10
		self.fullness -= 10
		cprint('{} подрал обои'.format(self.name), color='yellow')

	def act(self):
		if self.fullness <= 0:
			cprint('{} умер...'.format(self.name), color='red')
			return
		dice = randint(1, 6)
		if self.fullness < 40:
			self.eat()

		elif dice == 1:
			self.scratch()
		elif dice == 3:
			self.eat()
		else:
			self.sleep()


class Cat1(Cat):
	pass


class Cat2(Cat):
	pass


class Cat3(Cat):
	pass


class House:

	def __init__(self):
		self.cat_food = 0
		self.food = 50
		self.dirty = 0
		self.money = 2000

	def __str__(self):
		return 'В доме имеется {} еды, {} еды для кота, {} грязи и {} денег '.format(
			self.food, self.cat_food, self.dirty, self.money)


house = House()
cat = Cat(name='Барсик')
cat1 = Cat1(name='Бегемот')
cat2 = Cat2(name='Мурзик')
cat3 = Cat3(name='Толстопуз')
human = Human(name='Бедный Человек')
human.take_cat(house=house, cat=cat1)
cat1.get_home(house=house)
human.take_cat(house=house, cat=cat2)
cat2.get_home(house=house)
human.take_cat(house=house, cat=cat)
cat.get_home(house=house)



# cat3.get_home(house=house)

residents = [
	human,
	cat,
	cat1,
	cat2,

]
for day in range(1, 366):
	print('================ день {} =================='.format(day))
	for resident in residents:
		resident.act()
	print('---------------- в конце дня -------------------')
	for resident in residents:
		print(resident)
	print(house)

# Усложненное задание (делать по желанию)
# Создать несколько (2-3) котов и подселить их в дом к человеку.
# Им всем вместе так же надо прожить 365 дней.

# (Можно определить критическое количество котов, которое может прокормить человек...)
#  c тремя худо-бедно справляется, если имел запас денег, хоть и грязновато

# зачёт!
