# -*- coding: utf-8 -*-

# Есть файл с протоколом регистраций пользователей на сайте - registrations.txt
# Каждая строка содержит: ИМЯ ЕМЕЙЛ ВОЗРАСТ, разделенные пробелами
# Например:
# Василий test@test.ru 27
#
# Надо проверить данные из файла, для каждой строки:
# - присутсвуют все три поля
# - поле имени содержит только буквы
# - поле емейл содержит @ и .
# - поле возраст является числом от 10 до 99
#
# В результате проверки нужно сформировать два файла
# - registrations_good.log для правильных данных, записывать строки как есть
# - registrations_bad.log для ошибочных, записывать строку и вид ошибки.
#
# Для валидации строки данных написать метод, который может выкидывать исключения:
# - НЕ присутсвуют все три поля: ValueError
# - поле имени содержит НЕ только буквы: NotNameError (кастомное исключение)
# - поле емейл НЕ содержит @ и .(точку): NotEmailError (кастомное исключение)
# - поле возраст НЕ является числом от 10 до 99: ValueError
# Вызов метода обернуть в try-except.
class NotNameError(Exception):

    def is_name(self, name):
        # TODO - Функция строки isalpha способна проверить сразу всю строку, не надо по символам проверять
        x = 0
        for letter in name:
            if letter.isalpha() == False:
                x += 1

        if x > 0:
            return False

        else:
            return True

    def __str__(self):
        return '--------Ошибка в имени--------- \n'


not_name = NotNameError()


class NotEmailError(Exception):
    def is_email(self, email):
        # TODO - Исключения не используются для проверки, они выкидываются в результате проверки или неудачного действия
        email = str(email)
        if email.find('@') == -1 or email.find('.') == -1:
            return False
        else:
            return True

    def __str__(self):
        return '***Ошибка в Е-мэйле*** \n'


not_email = NotEmailError()


def is_age(age):
    '''

    возвращает Тру если возраст является числом от 10 до 99
    '''

    x = 0
    for i in age:
        if i.isdigit() == False:
            x += 1

    if x > 0:
        return False
    else:
        age = int(age)
        if age > 9 and age < 100:
            return True
        else:
            return False


with open('registrations.txt', 'r', encoding='UTF-8') as file, \
        open('registrations_good.log', 'a', encoding='UTF-8') as good_log, \
        open('registrations_bad.log', 'a', encoding='UTF-8') as bad_log:
    for line in file:
        line = line.strip()

        try:
            name, email, age = line.split(' ')
        except ValueError as val:
            bad_log.write(f'{line} - НЕ присутсвуют все три поля \n')
            # TODO - Если строка с ошибкой, нет смысла продолжать анализ строки, нужно переходить к следующей

        if not_name.is_name(name) and not_email.is_email(email) and is_age(age) and name and email and age:
            line2 = f'{name}  {email}  {age} \n'
            good_log.write(line2)
        # в этом варианте почему то дублируются некоторые строки в хорошем логе
        # TODO - Это потому, что на при "плохой" строке поля name, email, age не перезаписываются,
        #  значения остаются с предыдущего прохода "правильного"

        else:

            if not_name.is_name(name) == False:
                try:
                    raise NotNameError()
                except NotNameError as noname:
                    bad_log.write(f'{line} {noname} ')
            else:

                if not_email.is_email(email) == False:
                    try:
                        raise NotEmailError()
                    except NotEmailError as noem:
                        bad_log.write(f'{line} {noem} ')

                else:

                    if is_age(age) == False:
                        try:
                            raise ValueError
                        except ValueError:
                            bad_log.write(f'{line} Ошибка в возрасте \n')
