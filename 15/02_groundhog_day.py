# -*- coding: utf-8 -*-

# День сурка
#
# Напишите функцию one_day() которая возвращает количество кармы от 1 до 7
# и может выкидывать исключения:
# - IamGodError
# - DrunkError
# - CarCrashError
# - GluttonyError
# - DepressionError
# - SuicideError
# Одно из этих исключений выбрасывается с вероятностью 1 к 13 каждый день
#
# Функцию оберните в бесконечный цикл, выход из которого возможен только при накоплении
# кармы до уровня ENLIGHTENMENT_CARMA_LEVEL. Исключения обработать и записать в лог.
from random import randint

ENLIGHTENMENT_CARMA_LEVEL = 777
carma = 0
day = 0


def one_day():
	# TODO - Функция должна возвращать количество кармы для приращения, а не менять глобальную переменную
	global carma
	global day
	day += 1

	# TODO - Про это условие нет в задании
	if day == 365:
		carma = 777

	dice = randint(1, 13)
	if dice == 1:
		carma += 5
		raise BaseException('IamGodError')
	if dice == 3:
		carma += 2
		raise BaseException('DrunkError')

	if dice == 5:
		carma += 6
		raise BaseException('CarCrashError')

	if dice == 7:
		raise BaseException('GluttonyError')
		carma += 4
	if dice == 9:
		carma += 3
		raise BaseException('DepressionError')

	if dice == 11:
		carma += 1
		raise BaseException('SuicideError')
	else:
		carma += 1
		raise BaseException('Сегодня просто лягу спать')
	# TODO - По заданию нужн либо вернуть карму 1-7, либо выбросить исключение (6 вариантов)


while carma < ENLIGHTENMENT_CARMA_LEVEL:
	try:
		one_day()
	except  BaseException as exc:
		with open('log.txt', 'a', encoding='UTF-8') as log:
			log.write(f'День {day}: {carma} {exc} \n')
