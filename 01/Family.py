# Создайте списки:
from typing import Union

# моя семья (минимум 3 элемента, есть еще дедушки и бабушки, если что)
my_family = ["mother", "father", "wife", "dog"]

# список списков приблизителного роста членов вашей семьи
my_family_height = [
    # ['имя', рост],
    ["mother", 160],
    ["father", 180],
    ["wife", 170],
    ["dog", 100]
]
# Выведите на консоль рост отца
print(my_family_height[1][1])
# Выведите на консоль общий рост вашей семьи как сумму ростов всех членов
# TODO - Вот так со скобками можно записывать несколько элементов для лучшей читаемости, используя скобки
sum_height = (my_family_height[0][1]
              + my_family_height[1][1]
              + my_family_height[2][1]
              + my_family_height[3][1]
              )
print(sum_height)

# зачет!
