# Задание 2
# Напишите функцию для нахождения минимума в
# списке целых. Список передаётся в качестве параметра.
# Полученный результат возвращается из функции.
def comp(my_list):
  min = my_list[0]
  for j in range(1, len(my_list)):
    if min > my_list[j]:
      min = my_list[j]
  print(min)
  return min


comp([1, 2, 3, 0, 4, 5, -5])

# зачёт!
