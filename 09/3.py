# Задание 3
# Напишите функцию, определяющую количество про-
# стых чисел в списке целых. Список передаётся в качестве
# параметра. Полученный результат возвращается из функции.
def simple_count(my_list):
  real_counter = 0
  for item in my_list:
    counter = 0
    for x in range(1, item):
      if item % x != 0:
        counter += 1

      if item - counter == 2:
        real_counter += 1
  print(real_counter)
  return real_counter


simple_count([10, 11, 12, 13, 14, 15, 17, 19])

# зачёт!
