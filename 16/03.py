"""
Квадраты чисел
Что нужно сделать

Пользователь вводит число N. Напишите программу, которая генерирует последовательность из квадратов чисел
от 1 до N (1 ** 2, 2 ** 2, 3 ** 2 и так далее). Реализацию напишите тремя способами: класс-итератор,
функция-генератор и генераторное выражение.
"""
n = int(input('Введите число N----> '))


class My_pow:

	def __init__(self, n):
		self.i = 0
		self.n = n

	def __iter__(self):
		self.i = 0
		return self

	def __next__(self):
		self.i += 1

		if self.i > self.n:
			raise StopIteration()
		result = self.i ** 2
		return result


my_pow = My_pow(n)

for value in my_pow:
	print(value)
print('*' * 50)


def my_pow(n):
	for i in range(1, n + 1):
		i = i ** 2
		yield i


pow1 = my_pow(n)

for value in pow1:
	print(value)
print('*' * 50)

# TODO - В круглых скобках образуется генератор
# result = [x * x for x in range(1, n + 1)]
result = (x * x for x in range(1, n + 1))
print(result)

# зачёт!
