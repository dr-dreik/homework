import random

NUMBER_BUNCHES = 5
bunches = []


def init_bunches():
    for bunch in range(NUMBER_BUNCHES):
        bunches.append(random.randint(1, 20))


def get_from_bunch(bunch, stones):
    stones_in_bunch = bunches[bunch - 1]
    if stones_in_bunch >= stones:
        stones_in_bunch -= stones
        bunches[bunch - 1] = stones_in_bunch
        return True
    else:
        return False


def print_bunches():
    print(bunches)


def is_finish():
    return sum(bunches) == 0
