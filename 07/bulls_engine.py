import random

from termcolor import colored

print(colored('Приветствую в игре Быки и Коровы', 'yellow'))


def init_number():
  global machine_number
  while True:
    machine_number = (random.randint(1023, 9876))
    machine_number = str(machine_number)
    counter = 0

    for i in range(len(machine_number)):
      counter += int(machine_number.find(machine_number[i], i + 1))
    if counter == -len(machine_number):
      print('подсказка:', machine_number)
      return (machine_number)


def num_check(user_input):
  cows_count = 0
  bulls_count = 0
  if len(user_input) == 4:
    for j in range(len(user_input)):
      if user_input[j] == machine_number[j]:
        bulls_count += 1
        cows_count -= 1
      for i in user_input:
        if i == machine_number[j]:
          cows_count += 1

  result = {'коровы': cows_count, 'быки': bulls_count}
  return result


def is_win():
  print(colored("-------------------!!!!Победа!!!!--------------------", 'red'))
