#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Есть словарь координат городов

sities = {
  'Moscow': (550, 370),
  'London': (510, 510),
  'Paris': (480, 480),
}

# Составим словарь словарей расстояний между ними
# расстояние на координатной сетке - корень из (x1 - x2) ** 2 + (y1 - y2)**2

moscow = sities['Moscow']
london = sities['London']
paris = sities['Paris']

moscow_london = ((moscow[0] - london[0]) ** 2 + (moscow[1] - london[1]) ** 2) ** 0.5
moscow_paris = ((moscow[0] - paris[0]) ** 2 + (moscow[1] - paris[1]) ** 2) ** 0.5
paris_london = ((paris[0] - london[0]) ** 2 + (paris[1] - london[1]) ** 2) ** 0.5

distances = {
  'moscow_london': moscow_london,
  'moscow_paris': moscow_paris,
  'paris_london': paris_london,
  'london_moscow': moscow_london,
  'london_paris': paris_london,
  'paris_moscow': moscow_paris
}

print(distances)

# зачёт!
