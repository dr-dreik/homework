# Дан такой список:
#
# nice_list = [1, 2, [3, 4], [[5, 6, 7], [8, 9, 10]], [[11, 12, 13], [14, 15], [16, 17, 18]]]
#
# Напишите рекурсивную функцию, которая раскрывает все вложенные списки, то есть оставляет только внешний список.
#
# Ответ: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
sum_list = []
nice_list = [1, 2, [3, 4], [[5, 6, 7], [8, 9, 10]], [[11, 12, 13], [14, 15], [16, 17, 18]]]


# упс, кажется я уже написал ее в предыдущем задании.
def list_of_list(*args):
    for i in args:
        if type(i) == list:
            list_of_list(*i)
        else:
            sum_list.append(i)
    return sum_list


print(list_of_list(nice_list))

# зачёт!
